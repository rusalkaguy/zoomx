.. contents:: Navigator

Manual
========

1. Short Manuals for Python and R scripts are available with '-h' option
-------------------------------------------------------------------------

$ZOOMXPKG/zoomx/parse10x.py

.. code:: bash

  REPLACE_WITH_ZOOMX_PARSE10X_HELP

$ZOOMXPKG/zoomx/gridScan.py

.. code:: bash

  REPLACE_WITH_ZOOMX_GRIDSCAN_HELP

$ZOOMXPKG/zoomx/refineBreak.py

.. code:: bash

  REPLACE_WITH_ZOOMX_REFINEBREAK_HELP

$ZOOMXPKG/scripts/filterMolecule.R

.. code:: bash

  REPLACE_WITH_ZOOMX_FILTERMOLECULE_HELP

$ZOOMXPKG/scripts/plotMolecule.R

.. code:: bash

  REPLACE_WITH_ZOOMX_PLOTMOLECULE_HELP

2. Short Manuals for Bash scripts are available with comment lines or call wihtout options  
-------------------------------------------------------------------------------------------

$ZOOMXPKG/scripts/zoomxStage

.. code:: bash

  REPLACE_WITH_ZOOMX_ZOOMXSTAGE_HELP

$ZOOMXPKG/scripts/zoomxRun

.. code:: bash

  REPLACE_WITH_ZOOMX_ZOOMXRUN_HELP

$ZOOMXPKG/zoomx/getMolecule.sh

.. code:: bash

  REPLACE_WITH_ZOOMX_GETMOLECULE_HELP

$ZOOMXPKG/zoomx/bedpe2loupe.sh

.. code:: bash

  REPLACE_WITH_ZOOMX_BEDPE2LOUPE_HELP

